//Write a program to add two user input numbers using one function.
#include <stdio.h>
int main()
{
    int a,b,sum;
    printf("Enter two numbers that you need to add\n");
    scanf("%d%d",&a,&b);
    sum=a+b;
    printf("the sum of these 2 numbers %d,%d=%d\n",a,b,sum);
    return 0;
}
