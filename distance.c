//WAP to find the distance between two point using 4 functions.
#include<stdio.h>
#include<math.h>
float input()
{
   float a;
   printf("Enter the points in the form x1,y1,x2,y2\n");
   scanf("%f",&a);
   return a;
}
 
float find_dist(float x1,float x2,float y1,float y2)
{
   float dist,m;
   dist =((x2-x1)*(x2-x1))+((y2-y1)*(y2-y1));
   m = sqrt(dist);
   return m;
}
 
void output(float s)
{
   printf("the distance between the points you have given is %f",s);
}
 
int main()
{
   float x1,x2,y1,y2,d;
   x1=input();
   y1=input();
   x2=input();
   y2=input();
   d=find_dist(x1,x2,y1,y2);
   output(d);
   return 0;
}